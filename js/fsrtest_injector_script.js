
console.log("Injector Script entry");

//this script calls the fsr test function in fsrtest_injected_script.js'
//using let here causes an issue if used twice
var fsrTestInjectedScript = document.createElement('script');
fsrTestInjectedScript.src = chrome.extension.getURL('./js/fsrtest_injected_script.js');
fsrTestInjectedScript.onload = function() {
    this.remove();
};
(document.head || document.documentElement).appendChild(fsrTestInjectedScript);