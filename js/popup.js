/**
 * Created by Joshua Strickland & Brandt Bowling on 9/22/2017.
 */
//This is in the document of the extension

document.addEventListener('DOMContentLoaded', () => {

    var incognitoButton = document.getElementById('incognito-button');
    let url;
    //use query method to get the current tabs URL to insert into incognito window
    chrome.tabs.query({ active: true, currentWindow: true }, (tab) => {
        url = tab[0].url;
        console.log(url);
    });

    var criteriaButton = document.getElementById('criteria-finder-button');
    var adminPageButton = document.getElementById('admin-page-button');
    var copyTextareaBtn = document.getElementById('copy-btn');


    criteriaButton.addEventListener('click', () => {
        // See https://developer.chrome.com/extensions/tabs#method-executeScript.
        chrome.tabs.executeScript(null, { file: "./js/criteria_finder_injector_script.js" });
    });

    adminPageButton.addEventListener('click', () => {
        // See https://developer.chrome.com/extensions/tabs#method-executeScript.
        chrome.tabs.executeScript(null, { file: "./js/fsrtest_injector_script.js" });
    });

    incognitoButton.addEventListener('click', () => {
        //windows.create takes an options object.  Pass in url from tab and tell it to make an incognito window
        let windowOptions = {
            url: url,
            focused: true,
            incognito: true,
            state: "maximized"
        };
        chrome.windows.create(windowOptions);
    });
});



//Brandt, you magnificent bastard
chrome.runtime.onMessage.addListener(
    (request, sender, sendResponse) => {
        console.log(request);
        document.getElementById('criteria-display').innerHTML = request.data;
        sendResponse({ yay: "successfully received message" });
    });