/**
 * Created by Joshua Strickland & Brandt Bowling on 9/22/2017.
 */
//console.log("content_script entry");

//https://stackoverflow.com/questions/9515704/insert-code-into-the-page-context-using-a-content-script
//cant call code on page from exntension even after injecting
//can call code immediately after it is injected though
//this bit of script will inject the initial script onto the page
let initialScript = document.createElement('script');
// Be sure to add scripts used by page like "initial_script.js" to web_accessible_resources in manifest.json
initialScript.src = chrome.extension.getURL('./js/initial_script.js');
initialScript.onload = function () {
    this.remove();
};
(document.head || document.documentElement).appendChild(initialScript);

//this guy listens for an event from the page when button is pushed
//he passes the data from the page and passes it via an event on to popup.js
window.addEventListener("getChromeData", function (evt) {
    let request = evt.detail;
    // do Chrome things with request.data, add stuff to response.data
    chrome.runtime.sendMessage({ data: request.data }, (response) => {
        console.log(response.yay);
    });
    console.log(request);
}, false);
