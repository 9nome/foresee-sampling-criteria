
console.log("Injector Script entry");

//this script calls the criteria finder function in criteria_finder_injected_script.js'
//using let here causes an issue if used twice
var criteriaFinderInjectedScript = document.createElement('script');
criteriaFinderInjectedScript.src = chrome.extension.getURL('./js/criteria_finder_injected_script.js');
criteriaFinderInjectedScript.onload = function() {
    this.remove();
};
(document.head || document.documentElement).appendChild(criteriaFinderInjectedScript);