/**
 * Created by Joshua Strickland & Brandt Bowling on 9/22/2017.
 * This page is injected into a website-accessible location. This is done so the extension can call the functions defined here.
 */
console.log("Initializing Criteria Helper with CXCertainty");

let criteriaFinder = () => {
    //if FSR is undefined, we don't have code on site
    //so long as have FSR, let's go ahead
    if (window.FSR !== undefined) {
        //if getconfig is undefined, we are either on 18 or unsupported version
        if (window.FSR.getConfig === undefined) {
            //console.log("getConfig undefined, version is 18x/16x or unsupported");
            //call 18x func
            try {
                criteriaFinder18()
            } catch (e) {
                console.log("18x call failed, version is unsupported: " + e);

                let payload = {data: "Version is unsupported"};
                window.dispatchEvent(new CustomEvent("getChromeData", {detail: payload}));
            }
        } else {//FSR and getConfig is defined
            //console.log("getConfig defined, version is 19x");
            try {
                criteriaFinder19()
            } catch (e) {
                console.log("ForeSee Criteria Helper: criteriaFinder19 failed: " + e);
                let payload = {data: "Uh oh, please take a screenshot and reach out to support@foresee.com: " + e};
                window.dispatchEvent(new CustomEvent("getChromeData", {detail: payload}));
            }
        }
    } else {
        console.log("ForeSee Criteria Helper: ForeSee code not found");
        let textToShow = "ForeSee code not found";
        let payload = {data: textToShow};
        //this is the event that sends the data to the extension
        window.dispatchEvent(new CustomEvent("getChromeData", {detail: payload}));
    }
};

//Function that grabs criteria. This calls multiple helper functions to build a complete, HTML formatted string before sending it to the extension window
let criteriaFinder19 = () => {
    console.log("criteriaFinder19 entry");
    window.fsReady(function () {
        let FSRconfig = window.FSR.getConfig();
        let CPPs = getCPPString();
        let inviteExcludes = getIncludesExcludes(FSRconfig.config.inviteExclude);
        let globalExcludes = getIncludesExcludes(FSRconfig.config.globalExclude);
        let inviteType;
        let criteriaString = "<h2>Global Config</h2> <br>" +
            "<br><h4>CID:</h4> " + FSRconfig.config.id +
            "<br><h4>Repeat Days:</h4> Accept - " + FSRconfig.config.repeatDays.accept +
            " Decline - " + FSRconfig.config.repeatDays.decline +
            "<br> <h4>Invite Excludes:</h4>" + inviteExcludes +
            "<br> <h4>Global Excludes:</h4>" + globalExcludes +
            getBrowserCutoff(FSRconfig) +
            CPPs + "<br>";

        //survey def text creation loop
        let surveyDefString = "<br> <h2>Survey Definitions:</h2> <br>";
        surveyDefString += getActiveSurveyDef(FSRconfig);
        for (let i = 0; i < FSRconfig.surveydefs.length; i++) {
            inviteType = getInviteType(FSRconfig.surveydefs[i]);
            surveyDefString += "<h3>" + (i + 1) +
                ") Name: " + FSRconfig.surveydefs[i].name +
                getSection(FSRconfig.surveydefs[i]) + "</h3><br>" +
                " <h4>SP:</h4> " + FSRconfig.surveydefs[i].criteria.sp.reg +
                " <h4>LF:</h4> " + FSRconfig.surveydefs[i].criteria.lf + "<br>" +
                getSelectMode(FSRconfig.surveydefs[i].selectMode) +
                "<h4>Invite Type:</h4> " + inviteType + "<br>" +
                "<h4>Includes:</h4>" + getIncludesExcludes(FSRconfig.surveydefs[i].include) + "<br>" +
                "<h4>Excludes:</h4>" + getIncludesExcludes(FSRconfig.surveydefs[i].inviteExclude) + "<br><br>";
        }

        let textToShow = criteriaString + surveyDefString;
        /*example of sending events to content script
         https://stackoverflow.com/questions/33063774/communication-from-an-injected-script-to-the-content-script-with-a-response */
        let payload = {data: textToShow};
        //this is the event that sends the data to the extension
        window.dispatchEvent(new CustomEvent("getChromeData", {detail: payload}));

    });
};
//since our wonderful code decides every survey def needs mobile and desktop display configs we have to get the right one...
let getInviteType = (surveyDef) => {
    let platform;
    let inviteType;
    while (true) {

        if (surveyDef.criteria.supportsDesktop) {
            platform = 'desktop';
            break;
        } else if (surveyDef.criteria.supportsTablets) {
            platform = 'tablet';
            break;
        } else if (surveyDef.criteria.supportsSmartPhones) {
            platform = 'phone';
            break;
        }
    }
    try {
        switch (platform) {
            /*
             I had this really weird issue where parts of the FSRconfig object couldn't be found and it caused an error  when accessing the display object in this switch statement on the site when using the extension. For whatever reason, using incognito appears to have solved the issue.
             Seems testing in incognito is really important, but I don't know why this made a difference to be honest
             Brandt stumbled on to the idea that the definition you match at first will trigger a cache in the browser. The other definitions isplay property won't be in the cache on subsequent page loads.
             */
            case 'desktop':
                inviteType = surveyDef.display.desktop[0].inviteType;
                break;

            case 'tablet':
                inviteType = surveyDef.display.mobile[0].inviteType;
                break;

            case 'phone':
                inviteType = surveyDef.display.mobile[0].inviteType;
                break;

            default:
                return 'Could not find invite type.';
                break;
        }
    } catch (e) {
        console.log('Error finding invite type ' + e);
        return "We couldn't find the invite type. Try using an incognito window.";
    }
    return inviteType;
};

/*need a function to read through each array in the include/exclude list */
let getIncludesExcludes = (obj) => {
    let returnText = '';
    try {
        //URLs fetch
        if (obj.urls.length > 0) {
            returnText = '<br><h5> URLs: </h5><br>';
            for (i = 0; i < obj.urls.length; i++) {
                returnText += obj.urls[i];
                //only if multiple urls
                if (i + 1 < obj.urls.length) {
                    returnText += ", ";
                }
            }
            returnText += "<br>";
        }
        //Variables fetch
        if (obj.variables.length > 0) {
            returnText += "<br><h5> Variables: </h5><br>";
            for (i = 0; i < obj.variables.length; i++) {

                returnText += "<p class=blueText>Name: </p>" + obj.variables[i].name + " <p class=blueText>Value(s):</p> ";
                //need to add array for variable values, as multiple values can be used
                //looks like if the include val is a single string, it prints each character as part of the string array, so I need to use a conditional statement to check for strings

                //Need to check for if val is defined, because this can optionally replace value
                if (obj.variables[i].val !== undefined) {
                    obj.variables[i].value = obj.variables[i].val;
                }

                try {//makes sure there is a value
                    //if we ahve an array...
                    if (typeof obj.variables[i].value !== "string" && typeof obj.variables[i].value !== "boolean" && typeof obj.variables[i].value !== "number") {
                        //iterate through multiple values
                        for (j = 0; j < obj.variables[i].value.length; j++) {
                            returnText += obj.variables[i].value[j] /*+ ", "*/;
                            //only add comma if there are more values
                            if (j + 1 < obj.variables[i].value.length) {
                                returnText += ", ";
                            }
                        }

                        //as long as we don't have an array...(there is probably a better check for this)
                    } else if (typeof obj.variables[i].value === "string" || typeof obj.variables[i].value === "boolean" || typeof obj.variables[i].value === "number") {
                        returnText += obj.variables[i].value;
                    }
                } catch (e) {
                    returnText += " No Variable Value";
                }
                //in case of multiple variables
                if (i + 1 < obj.variables.length) {
                    returnText += " <br>";
                }
            }
            returnText += "<br>";
        }
        //Cookies fetch
        if (obj.cookies.length > 0) {
            returnText += "<br><h5> Cookies: </h5><br>";
            for (i = 0; i < obj.cookies.length; i++) {
                returnText += "<p class=blueText>Name: </p>" + obj.cookies[i].name + " <p class=blueText>Value: </p>";

                //if "val" exists
                if (obj.cookies[i].val !== undefined) {
                    obj.cookies[i].value = obj.cookies[i].val;
                }

                //makessure there is a value
                if (obj.cookies[i].value !== undefined) {

                    try {//this try catch didn't work for some reason, so am using if else depedning on undefined datatype
                        returnText += obj.cookies[i].value;
                    } catch (e) {
                        returnText += "No Cookie Value";
                    }
                } else {
                    returnText += "No Cookie Value";
                }
                if (i + 1 < obj.cookies.length) {
                    returnText += " <br>";
                }
            }
            returnText += "<br>";
        }

        if (obj.userAgents.length > 0) {
            returnText += "<br> User Agents: <br>";
            for (i = 0; i < obj.userAgents.length; i++) {

                returnText += obj.userAgents[i];
                if (i + 1 < obj.userAgents.length) {
                    returnText += ", <br>";
                }
            }
            returnText += "<br>";
        }

        if (obj.browsers.length > 0) {
            returnText += "<br> Browsers: <br>";
            for (i = 0; i < obj.browsers.length; i++) {
                //returnText += obj.browsers[i].name + ": " + obj.browsers[i].version + ", ";
                //can't find client with browser includes / excludes using stringify for safety.
                returnText += JSON.stringify(obj.browsers[i]);
            }
            returnText += "<br>";
        }

        if (obj.referrers.length > 0) {
            returnText += "<br> Referrers: <br>";
            for (i = 0; i < obj.referrers.length; i++) {
                returnText += obj.referrers[i] + ", ";
                if (i + 1 < obj.referrers.length) {
                    returnText += ", <br>";
                }
            }
            returnText += "<br>";
        }

    } catch (e) {
        console.log("ForeSee Criteria Helper: getIncludeExcludes failed: " + e);
        returnText += "Uh oh, please take a screenshot and reach out to support@foresee.com: " + e;
    }
    //console.log(returnText);
    return returnText;

};

let getSelectMode = (obj) => {
    let textToShow = "";
    if (obj !== "default") {
        textToShow = "<h3>Select Mode:</h3> " + obj + "<br>";
    }
    return textToShow;
};

/* The storage.get function apparently takes a callback function that can alternatively be console.log
 Figured this out by using
 https://stackoverflow.com/questions/15462122/assign-console-log-value-to-a-variable */
let getCPPString = () => {
    try {
        FSR.Storage.get('cp', function (val) {
            window.cppHolder = val
        });
        //this works but i want to stringify the whole thing now
        //var CPPs = window.cppHolder;
        //return CPPs;
        //using regex here to remove commas / {}
        let returnText = "<br> <h2>CPPs </h2><br>" + JSON.stringify(cppHolder);
        returnText = returnText.replace(/,/g, "<br>");
        returnText = returnText.replace(/["{}]/g, "");
        return returnText;
    } catch (e) {
        console.log("ForeSee Criteria Helper: CPPs won't work, version is too low: " + e);
    }
};

let getBrowserCutoff = (config) => {
    try {
        let returnText = "<br> <h4>Browser Cutoff: </h4><br>" + JSON.stringify(config.config.browser_cutoff);
        returnText = returnText.replace(/,/g, "<br>");
        returnText = returnText.replace(/["{}]/g, "");
        return returnText;
    } catch (e) {
        console.log("ForeSee Criteria Helper: Browser cutoff won't work, version is too low: " + e);
    }
};

let getSection = (obj) => {
    let returnText = '';
    if (obj.section != null && obj.section != undefined) {
        returnText = ", Section: " + obj.section;
    }
    return returnText;
};

let launchAdminPage = () => {
    window.FSR.test();
};

//Hey this also works for 16x
let criteriaFinder18 = () => {
    let textToShow = "";

    if (FSR.prop.repeatdays !== undefined && FSR.prop.repeatdays !== null) {

        textToShow += "<h3>Global: </h3>" +
            "<br><h4>Repeat Days:</h4> " + FSR.prop.repeatdays;
    }
    let surveydefs = FSR.surveydefs;
    let surveyDefString = "<br> <h3>Survey Definitions:</h3> <br>";
    for (let i = 0; i < surveydefs.length; i++) {
        surveyDefString += "<h3>" + (i + 1) +
            ": Name: " + surveydefs[i].name +
            getSection18(surveydefs[i]) +
            " <br><h4>SP:</h4> " + surveydefs[i].criteria.sp +
            ", <h4>LF:</h4> " + JSON.stringify(surveydefs[i].criteria.lf) +
            "<br>";

    }
    textToShow += surveyDefString;

    let payload = {data: textToShow};
    window.dispatchEvent(new CustomEvent("getChromeData", {detail: payload}));
};

let getSection18 = (surveyDef) => {
    let returnText = '';
    if (surveyDef.section !== undefined) {

        returnText += ": Section: " + surveyDef.section;
    }
    return returnText;
};

let getActiveSurveyDef = (config) => {
    let returnText = "";
    try {
        returnText += "<h3>Active Survey Definition: Name: " + config.active_surveydef.name;
        returnText += getSection(config.active_surveydef);
        returnText += "</h3><br>"
    } catch (e) {
        console.log("ForeSee Criteria Helper: Can't get active survey def" + e);
    }
    return returnText;
};
